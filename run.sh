#! /bin/bash

BASEDIR=$(dirname $0)

if [ ! -f /tmp/tmp-chromium ]; then
mkdir -p /tmp/tmp-chromium
fi

if [ "${DEBUG}" == 'y' ]; then
$BASEDIR/latest/chrome --user-data-dir="$BASEDIR/user-data-dir" --disk-cache-dir=/tmp/tmp-chromium --disable-field-trial-config --disable-infobars --force-local-ntp $*
else
$BASEDIR/latest/chrome --user-data-dir="$BASEDIR/user-data-dir" --disk-cache-dir=/tmp/tmp-chromium --disable-field-trial-config --disable-infobars --force-local-ntp $* &> /dev/null
fi
rm user-data-dir/Default/{"Cookies","Favicons","History","Last Session","Last Tabs","Network Action Predictor","Shortcuts","Top Sites"}

rm -Rf /tmp/tmp-chromium/
